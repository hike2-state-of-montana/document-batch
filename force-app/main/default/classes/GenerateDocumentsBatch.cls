public without sharing class GenerateDocumentsBatch implements Database.Batchable<SObject>, Database.AllowsCallouts{
    List<SObject> objects;
    List<String> toEmails;
    String objectName;
    webm__Webmerge_Mapping__c formstackMapping;
    String fileName;
    EmailTemplate template;
    orgWideEmailAddress fromEmail;
    public GenerateDocumentsBatch(List<SObject> objects, webm__Webmerge_Mapping__c formstackMapping, List<String> toEmails, String ObjectName, EmailTemplate template, OrgWideEmailAddress fromEmail){
        this.objects = objects;
        this.toEmails = new List<String>(toEmails);
        this.objectName = ObjectName;
        this.formstackMapping = formstackMapping;
        this.template = template;
        this.fromEmail = fromEmail;
    }

    public Iterable<SObject> start(Database.BatchableContext BC){
      return this.objects;
    }

    public void execute(Database.BatchableContext BC, List<SObject> scope){
        List<String> ids = new List<String>();
        for(SObject o: scope){
            webm.WebmergeGenerator.generateDocument(
                this.formstackMapping.Id,
                o.Id,
                this.ObjectName
            );
        }
    }

    public void finish(Database.BatchableContext BC){
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        email.setSubject(this.template.subject);
        String htmlBody = this.template.HTMLValue.replace('{COUNT}', String.valueOf(this.objects.size()));
        email.setHtmlBody(htmlBody);
        email.setToAddresses(new List<String>(this.toEmails));
        email.setOrgWideEmailAddressId(this.fromEmail.Id);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[]{email});
    }
}