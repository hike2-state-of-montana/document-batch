global without sharing class DocumentUtility {
    @InvocableMethod(label='Generate & Combine Documents' description='Merge each sobject to its document then create a single pdf of all the documents and email it to the user. Do NOT use in a scheduled flow.')
    global static void generateAndCombineDocuments(List<DocumentRequest> requests){
        for(DocumentRequest request: requests){
            Database.executeBatch(
                new GenerateAndCombineDocumentsBatch(
                    request.objects, 
                    request.formstackMapping, 
                    request.Emails, 
                    request.objects[0].getSObjectType().getDescribe().getName(), 
                    request.fileName,
                    request.template
                    ),5
                );
        }
    }
    global static Pagereference mergeAttachments(List<Attachment> attachments){
        List<Id> body = new List<Id>();
        for(Attachment a: attachments){
            body.add(a.Id);
        }
        String at = JSON.serialize(body);
        return new PageReference('/apex/AttachmentsPage?letters='+at);
    }
    global class DocumentRequest{
        @InvocableVariable(required=true)
        global List<SObject> objects;
        @InvocableVariable(required=true)
        global Id formstackMapping;
        @InvocableVariable(required=true)
        global List<String> Emails;
        @InvocableVariable(required=true)
        global String fileName;
        @InvocableVariable(required=true)
        global Id template;
    }
}
