public without sharing class GenerateAndCombineDocumentsBatchNoAtt implements Database.Batchable<SObject>, Database.Stateful, Database.AllowsCallouts, Database.RaisesPlatformEvents{
    List<SObject> objects;
    List<Attachment> attachments;
    User toUser;
    String objectName;
    webm__Webmerge_Mapping__c formstackMapping;
    String fileName;
    EmailTemplate template;
    Integer maxSize;
    orgWideEmailAddress fromEmail;
    public GenerateAndCombineDocumentsBatchNoAtt(List<SObject> objects, webm__Webmerge_Mapping__c formstackMapping, User toUser, String ObjectName, String fileName, EmailTemplate template, OrgWideEmailAddress fromEmail){
        this.attachments = new List<Attachment>();
        this.objects = objects;
        this.toUser = toUser;
        this.fileName = fileName;
        this.objectName = ObjectName;
        this.formstackMapping = formstackMapping;
        this.template = template;
        this.fromEmail = fromEmail;
        this.maxSize = 50;
    }

    public Iterable<SObject> start(Database.BatchableContext BC){
      return this.objects;
    }

    public void execute(Database.BatchableContext BC, List<SObject> scope){
        List<Attachment> attch = new List<Attachment>();
        for(SObject o: scope){
            List<Attachment> atchmts = webm.WebmergeGenerator.generateDocumentGetAttachments(
                this.formstackMapping.Id,
                o.Id,
                this.ObjectName
            );
            for(Attachment at: atchmts){
                at.OwnerId = toUser.Id;
                attch.add(at);
            }
        }
        insert attch;
        for(Attachment at: attch){
            this.attachments.add(new Attachment(Id=at.Id));
        }
    }

    public void finish(Database.BatchableContext BC){
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        List<String> refNames = new List<String>();
        List<Messaging.EmailFileAttachment> emailattachments = new List<Messaging.EmailFileAttachment>();
        Integer nameNumber = 0;
        //AsyncApexJob a = [Select Id, Status,ExtendedStatus,NumberOfErrors, JobItemsProcessed,TotalJobItems, CreatedBy.Email    from AsyncApexJob where Id =:BC.getJobId()];
        //List<Document_Set__e> docsets = new List<Document_Set__e>();
        for(Integer idx = 0; idx < this.attachments.size();){
            //Document_Set__e docset = new Document_Set__e();
            List<Attachment> crAttachments = new List<Attachment>();
            for(Integer i = 0; idx < this.attachments.size() && i < (this.maxSize); idx++){
                crAttachments.add(this.attachments.get(idx));
                i++;
            }
            if(crAttachments.size() > 0 ){
                Messaging.emailFileAttachment emailattachment = new Messaging.emailFileAttachment();
                emailattachment.setFileName(nameNumber == 0 ? this.fileName + '.pdf' : this.fileName + nameNumber + '.pdf');  
                System.debug(crAttachments);
                List<Id> ids = new List<Id>();
                for(Attachment at: crAttachments){
                    ids.add(at.Id);
                }
                String ref = '/apex/AttachmentsPage?letters=' + DocumentUtilityNoAttachments.StringThisArray(ids);
                ref = ref.replaceAll('\\(', '%5B%22');
                ref = ref.replaceAll(', ', '%22%2C%22');
                ref = ref.replaceAll('\\)', '%22%5D');
                refNames.add(ref);
                /**emailattachment.setContentType('application/pdf');
                emailattachment.setInline(false);
                Blob content = ref.getContentAsPDF();
                emailattachment.setBody(content);
                emailattachments.add(emailattachment);*/
            }
            nameNumber++;
        }
        //if(docsets.size() > 0)
            //EventBus.publish(docsets);
        String htmlBody = this.template.htmlValue;
        String fullBody = '';
        for(String line: htmlBody.split('\n')){
            Integer startstr = line.indexOf('{R}');
            Integer endstr = line.indexOf('{/R}');
            if(line.contains('{Link}') || (line.contains('{R}') && line.contains('{/R}'))){
                String lines = '';
                Integer i = 0;
                for(String ref: refNames){
                    String crlinerepeat = line.substring(startstr, endstr).replace('{Link}', ref);
                    if(i > 0){
                        crlinerepeat = crlinerepeat.replace('{i}', ' part ' + (i+1));
                    }else{
                        crlinerepeat = crlinerepeat.replace('{i}', '');
                    }
                    lines += crlinerepeat;
                    i++;
                }
                line = line.replace(line.substring(startstr, endstr), lines);
            }
            line = line.replace('{R}', '');
            line = line.replace('{/R}', '');
            fullBody += line;
        }
        email.setSubject(this.template.Subject);
        email.setHtmlBody(fullBody);
        email.setToAddresses(new String[]{this.toUser.Id});
        email.setOrgWideEmailAddressId(this.fromEmail.Id);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[]{email});
    }
}