public without sharing class GenerateAndCombineDocumentsBatch implements Database.Batchable<SObject>, Database.Stateful, Database.AllowsCallouts, Database.RaisesPlatformEvents{
    List<SObject> objects;
    List<Attachment> attachments;
    List<String> Emails;
    String objectName;
    Id formstackMapping;
    String fileName;
    Id template;
    Integer maxSize;
    public GenerateAndCombineDocumentsBatch(List<SObject> objects, Id formstackMapping, List<String> Emails, String ObjectName, String fileName, Id template){
        this.attachments = new List<Attachment>();
        this.objects = objects;
        this.Emails = Emails;
        this.fileName = fileName;
        this.objectName = ObjectName;
        this.formstackMapping = formstackMapping;
        this.template = template;
        this.maxSize = 50;
    }

    public Iterable<SObject> start(Database.BatchableContext BC){
      return this.objects;
    }

    public void execute(Database.BatchableContext BC, List<SObject> scope){
        List<Attachment> attch = new List<Attachment>();
        for(SObject o: scope){
            List<Attachment> atchmts = webm.WebmergeGenerator.generateDocumentGetAttachments(
                this.formstackMapping,
                o.Id,
                this.ObjectName
            );
        }
        insert attch;
        for(Attachment at: attch){
            this.attachments.add(new Attachment(Id=at.Id));
        }
    }

    public void finish(Database.BatchableContext BC){
        EmailTemplate EmailTemplate = [SELECT Id, Subject, HTMLValue FROM EmailTemplate WHERE ID = :this.template LIMIT 1];
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        List<String> refNames = new List<String>();
        List<Messaging.EmailFileAttachment> emailattachments = new List<Messaging.EmailFileAttachment>();
        Integer nameNumber = 0;
        //AsyncApexJob a = [Select Id, Status,ExtendedStatus,NumberOfErrors, JobItemsProcessed,TotalJobItems, CreatedBy.Email    from AsyncApexJob where Id =:BC.getJobId()];
        //List<Document_Set__e> docsets = new List<Document_Set__e>();
        for(Integer idx = 0; idx < this.attachments.size();){
            //Document_Set__e docset = new Document_Set__e();
            List<Attachment> crattachments = new List<Attachment>();
            for(Integer i = 0; idx < this.attachments.size() && i < (this.maxSize); idx++){
                crattachments.add(this.attachments.get(idx));
                i++;
            }
            if(crAttachments.size() > 0 ){
                Messaging.emailFileAttachment emailattachment = new Messaging.emailFileAttachment();
                emailattachment.setFileName(nameNumber == 0 ? this.fileName + '.pdf' : this.fileName + nameNumber + '.pdf');    
                Pagereference ref = documentUtility.mergeAttachments(crattachments);
                emailattachment.setContentType('application/pdf');
                emailattachment.setInline(false);
                Blob content = ref.getContentAsPDF();
                emailattachment.setBody(content);
                emailattachments.add(emailattachment);
            }
            nameNumber++;
        }
        //if(docsets.size() > 0)
            //EventBus.publish(docsets);
        String htmlBody = EmailTemplate.htmlValue;
        String fullBody = '';
        for(String line: htmlBody.split('\n')){
            System.debug(line);
            Integer startstr = line.indexOf('{R}');
            Integer endstr = line.indexOf('{/R}');
            if(line.contains('{Link}') || (line.contains('{R}') && line.contains('{/R}'))){
                String lines = '';
                for(String ref: refNames){
                    String crlinerepeat = line.substring(startstr, endstr).replace('{Link}', ref);
                    lines += crlinerepeat + '\n';
                }
                line = line.replace(line.substring(startstr, endstr), lines);
            }
            line = line.replace('{R}', '');
            line = line.replace('{/R}', '');
            fullBody += line;
        }
        System.debug(fullBody);
        System.debug(emailattachments);
        email.setSubject(EmailTemplate.Subject);
        email.setReplyTo('apexbatchprocess@no-reply.com');
        email.setSenderDisplayName('Generated Documents');
        email.setHtmlBody(fullBody);
        email.setFileAttachments(emailattachments);
        email.setToAddresses(new List<String>(this.Emails));

        Messaging.sendEmail(new Messaging.SingleEmailMessage[]{email});
    }
}