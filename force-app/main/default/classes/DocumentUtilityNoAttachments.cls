global without sharing class DocumentUtilityNoAttachments {
    @InvocableMethod(label='Generate & Combine Documents No Attachments' description='Meant to be used in scheduled flow.')
    global static void generateAndCombineDocumentsNoAtt(List<DocumentRequest> requests){
        for(DocumentRequest request: requests){
            Database.executeBatch(
                new GenerateAndCombineDocumentsBatchNoAtt(
                    request.objects, 
                    request.formstackMapping, 
                    request.toUser, 
                    request.objects[0].getSObjectType().getDescribe().getName(), 
                    request.fileName,
                    request.template,
                    request.orgEmail
                    ),5
                );
        }
    }
    global static String StringThisArray(List<Id> arr){
        String retval = '(';
        for(Id ob: arr){
           retval += ob + ', ';
        }
        retval = retval.removeEnd(', ');
        retval += ')';
        return retval;
    }
    global class DocumentRequest{
        @InvocableVariable(required=true)
        global List<SObject> objects;
        @InvocableVariable(required=true)
        global webm__Webmerge_Mapping__c formstackMapping;
        @InvocableVariable(required=true)
        global User toUser;
        @InvocableVariable(required=true)
        global String fileName;
        @InvocableVariable(required=true)
        global EmailTemplate template;
        @InvocableVariable(required=true)
        global OrgWideEmailAddress orgEmail;
    }
}
