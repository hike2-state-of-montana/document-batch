global without sharing class DocumentUtilityNoCombine {
    @InvocableMethod(label='Generate Documents in Batch' description='Meant to be used in scheduled flow.')
    global static void generateDocuments(List<DocumentRequest> requests){
        for(DocumentRequest request: requests){
            Database.executeBatch(
                new GenerateDocumentsBatch(
                    request.objects, 
                    request.formstackMapping, 
                    request.emails, 
                    request.objects[0].getSObjectType().getDescribe().getName(), 
                    request.template,
                    request.orgEmail
                    ),5
                );
        }
    }
    global class DocumentRequest{
        @InvocableVariable(required=true)
        global List<SObject> objects;
        @InvocableVariable(required=true)
        global webm__Webmerge_Mapping__c formstackMapping;
        @InvocableVariable(required=true)
        global List<String> emails;
        @InvocableVariable(required=true)
        global EmailTemplate template;
        @InvocableVariable(required=true)
        global OrgWideEmailAddress orgEmail;
    }
}
