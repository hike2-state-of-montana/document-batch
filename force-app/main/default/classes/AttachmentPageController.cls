public with sharing class AttachmentPageController {
    public List<Attachment> letters {get; set;}
    //List<Id> Ids = new List<Id>{'00P3S000000IlkzUAC', '00P3S000000IlkyUAC', '00P3S000000Ill3UAC'};
    public AttachmentPageController() {
        this.letters = new List<Attachment>();
        for(Id atid: (List<Id>) JSON.deserialize(apexpages.currentPage().getParameters().get('letters'), List<Id>.class))
            this.letters.add(new Attachment(Id=atid));
    }
}
